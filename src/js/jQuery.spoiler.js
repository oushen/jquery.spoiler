/**
 * jQuery.spoiler plugin
 * @author Polyakov Andrey
 * @license The MIT License (MIT)
 * @site https://bitbucket.org/oushen/jquery.spoiler
 */
;(function($, window, document, undefined) {
  
  /**
   * Creates a spooler.
   * @class The jquerySpoiler.
   * @public
   * @param {HTMLElement|jQuery} element - The element to create the carousel for.
   * @param {Object} [options] - The options
   */
  function jquerySpoiler(element, options) {
    /**
     * Spoiler settings.
     * @public
     */
    this.settings = $.extend({}, jquerySpoiler.Defaults, options);
    
    /**
     * Plugin element.
     * @public
     */
    this.$element = $(element);
    
  };
  
  /**
   * Initializes the jquerySpoiler.
   * @protected
   */
  jquerySpoiler.prototype.init = function() {
    
  };
  
  /**
   * Default options for the jQuery Spoiler plugin.
   * @public
   */
  jquerySpoiler.Defaults = {
    
  };
  
  /**
   * The jQuery Plugin for the Spoiler
   * @public
   */
  $.fn.spoiler = function(options) {
    var args = Array.prototype.slice.call(arguments, 1);

    if (typeof option == 'object') {
      options = options || jquerySpoiler.Defaults;
    }
    
    return this.each(function() {
      
      var $this = $(this),
          jquerySpoiler = $this.data('jqSpoiler');

      if (typeof options == 'string' && jquerySpoiler != undefined) {
        jquerySpoiler[options].apply(jquerySpoiler, args);
      }
      else if (typeof options == 'string' && jquerySpoiler != undefined) {
        console.warn('jquerySpoiler not initialized for element - cannot execute requested method.');
      }
      else if (typeof options == 'object') {
        stickedblock = new jquerySpoiler($this, options);
        stickedblock.init();
      }
    }); 
  };
  
})(window.jQuery, window, document);